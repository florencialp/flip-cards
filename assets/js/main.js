var tvShowsResult;
let movies = document.getElementById("movies");
let cardsContainer = document.getElementById("contenedor");
let tvShows;


window.onload = loadDoc("http://api.tvmaze.com/shows", getCards);

function loadDoc(url, cFunction) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            tvShowsResult = this.responseText;
            tvShows = JSON.parse(tvShowsResult);

            cFunction(this);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}


let btnSearch = document.getElementById("btnSearch");

btnSearch.addEventListener("click", function(evento) {
    evento.preventDefault();
    searchTvShows();
});

function deleteTvShows() {
    cardsContainer.innerHTML = "";

}

function searchTvShows() {
    deleteTvShows();

    let url = "http://api.tvmaze.com/search/shows?q=";
    let inputIngresado = document.getElementById("input").value;

    let urlCompleta = url + inputIngresado;
    loadDoc(url, mostrartvShows);
}


function getCards(b) {
    deleteTvShows();

    let inicio, fin, cantidad;

    tvShowsLength = JSON.parse(tvShowsResult).length;
    cantidad = tvShowsLength / 8;

    if (b.id == "1" || b.id == undefined) {
        inicio = 0
        fin = cantidad

    } else {
        inicio = (cantidad * b.id) - cantidad
        fin = cantidad * b.id
    }
    iteratePagination(inicio, fin);
}

function iteratePagination(inicio, fin) {
    for (let i = 0; i < tvShows.length; i++) {

        if (i == inicio) {
            for (let a = inicio; a >= inicio && a <= fin; a++) {
                loadCards(a, cardsContainer);
            }
        }
    }
    scrollTo(0, 0);
}

function loadCards(i, cardsContainer) {

    //se crean los elementos del frende de la card
    let flipCard = document.createElement("div");
    flipCard.className += "flip-card";

    let flipCardInner = document.createElement("div");
    flipCardInner.className += "flip-card-inner";

    let flipCardFront = document.createElement("div");
    flipCardFront.className += "flip-card-front";

    let img = document.createElement("img");

    img.setAttribute("src", tvShows[i].image.original);


    let cardImgOverlay = document.createElement("div");
    cardImgOverlay.className += "card-img-overlay";

    let cardTitle = document.createElement("h5");
    cardTitle.innerText += tvShows[i].name;

    //se appendean los elementos del frente de la card
    contenedor.appendChild(flipCard);
    flipCard.appendChild(flipCardInner);
    flipCardInner.appendChild(flipCardFront);
    flipCardFront.appendChild(img);
    flipCardFront.appendChild(cardImgOverlay);
    cardImgOverlay.appendChild(cardTitle);


    //se crean los elementos del reverso de la card
    let flipCardBack = document.createElement("div");
    flipCardBack.className += "flip-card-back";

    let cardDetail = document.createElement("div");
    cardDetail.className += "card-detail";

    let movieName = document.createElement("h3");
    movieName.innerText += tvShows[i].name;

    let score = document.createElement("div");
    score.className += "score";

    let scoreEmoji = document.createElement("div");
    scoreEmoji.innerHTML += '<i class="fas fa-star"></i>';

    let rating = document.createElement("p");
    rating.innerText += tvShows[i].rating.average;

    let movieSummary = document.createElement("p");
    movieSummary.innerHTML += tvShows[i].summary;

    let btnContainer = document.createElement("div");
    btnContainer.className += "btn-container";

    let url = document.createElement("a");
    url.innerHTML += "Ver más";
    url.setAttribute("href", tvShows[i].url);
    url.setAttribute("target", "_blank");


    //se appendean los elementos del reverso de la card
    flipCardInner.appendChild(flipCardBack);
    flipCardBack.appendChild(cardDetail);
    cardDetail.appendChild(movieName);
    cardDetail.appendChild(score);
    score.appendChild(scoreEmoji);
    score.appendChild(rating);
    cardDetail.appendChild(movieSummary);
    cardDetail.appendChild(btnContainer);
    btnContainer.appendChild(url);
}